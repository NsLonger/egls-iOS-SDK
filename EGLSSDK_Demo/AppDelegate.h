
#import "EGLSSDK.h"
#import <CoreMotion/CoreMotion.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate, EGLSSDKDelegate,UITextFieldDelegate>
{
     CMMotionManager * selfMotionManager ;
}
@property (strong, nonatomic) UIWindow * window;
@property (retain, nonatomic) IBOutlet UITextField * accountField;
@property (retain, nonatomic) IBOutlet UITextField * verifyField;
@property (retain, nonatomic) IBOutlet UITextField * passWordField;
+ (UIViewController *)getCurrentVC;
- (void) motionTest;
- (void) motionTest1;
-(void) closeMotion;
-(UITextField *) getUITextField:(CGRect)rectMake placeholder:(NSString*)placeholder;
@end
